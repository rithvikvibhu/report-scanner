import PyPDF2
import re
import xml.etree.cElementTree as ET
import os

## Init
probs = []
foldername = "."  # Same folder as script (.)
root = ET.Element("data")
students = ET.SubElement(root, "students")

filesList = os.listdir(foldername)
filesList.remove(os.path.basename(__file__))  # Exclude self from list of files
filesList.remove('probs.txt')                 # Exclude probs.txt from list of files
filesList.remove('out.xml')                   # Exclude out.xml from list of files
filesList.remove('out-sorted.xml')            # Exclude out-sorted.xml from list of files

for filename in filesList:


    ## Open PDF
    print "FILE: ", foldername, filename
    file = open(foldername+'/'+filename,"rb")
    pdfReader = PyPDF2.PdfFileReader(file)


    ## Extract content
    pageObj = pdfReader.getPage(0)
    page_content = pageObj.extractText()
    trimmed_page_content = page_content#[289:-56]  # Not using this as this doesn't work with all PDFs (N,E,S)
    print trimmed_page_content                     # DEBUG: Print page content.


    ## Reset variables (so that previous student's details don't get used.)
    student_name = ":"
    student_class = ":"
    student_dob = ":"
    student_id = ":"
    student_address = ":"
    student_mother = ":"
    student_father = ":"
    student_telephone = ":"
    student_email = ":"


    ## Variables set

    try:
        if pdfReader.getNumPages() >= 2:
            student_name = trimmed_page_content[trimmed_page_content.index('Name of '):trimmed_page_content.index('Section')].split(':')[1].title()
        elif pdfReader.getNumPages() == 1:
            student_name = trimmed_page_content[trimmed_page_content.index('Name of'):trimmed_page_content.index('Admission No.')].split(':')[1].title()
    except Exception as e:
        print "ERROR in student_name: ", str(e), "\n\n"
        probs.append(filename+" (student_name): "+str(e))
        pass

    try:
        student_class = trimmed_page_content[trimmed_page_content.index('Section'):trimmed_page_content.index('Date of ')].split(':')[1]
    except Exception as e:
        print "ERROR in student_class: ", str(e), "\n\n"
        probs.append(filename+" (student_class): "+str(e))
        pass

    try:
        student_dob = trimmed_page_content[trimmed_page_content.index('Date of '):trimmed_page_content.index('Date of ')+25].split(':')[1]
    except Exception as e:
        print "ERROR in student_dob: ", str(e), "\n\n"
        probs.append(filename+" (student_dob): "+str(e))
        pass

    try:
        if pdfReader.getNumPages() >= 2:
            student_id = trimmed_page_content[trimmed_page_content.index('Admission No'):trimmed_page_content.index('Residential Address')].split(':')[1]
        elif pdfReader.getNumPages() == 1:
            student_id = trimmed_page_content[trimmed_page_content.index('Admission No'):trimmed_page_content.index('Section')].split(':')[1]
    except Exception as e:
        print "ERROR in student_id: ", str(e), "\n\n"
        probs.append(filename+" (student_id): "+str(e))
        pass

    try:
        student_address = trimmed_page_content[trimmed_page_content.index('Residential Address'):trimmed_page_content.index('Mother')].split(':')[1].title()
    except ValueError:
        student_address = ""
    except Exception as e:
        print "ERROR in student_address: ", str(e), "\n\n"
        probs.append(filename+" (student_address): "+str(e))
        pass

    try:
        student_mother = trimmed_page_content[trimmed_page_content.index('Mother'):trimmed_page_content.index('Father')-1].split(':')[1].title()
    except ValueError:
        student_mother = ""
    except Exception as e:
        print "ERROR in student_mother: ", str(e), "\n\n"
        probs.append(filename+" (student_mother): "+str(e))
        pass

    try:
        student_father = trimmed_page_content[trimmed_page_content.index('Father'):trimmed_page_content.index('Telephone Number')].split(':')[1].title()
    except ValueError:
        try:
            student_father = trimmed_page_content[trimmed_page_content.index('Father'):trimmed_page_content.index('Attendance')].split(':')[1].title()
        except ValueError:
            student_father = ""
    except Exception as e:
        print "ERROR in student_father: ", str(e), "\n\n"
        probs.append(filename+" (student_father): "+str(e))
        pass

    try:
        student_telephone = trimmed_page_content[trimmed_page_content.index('Telephone Number'):trimmed_page_content.index('E-Mail ID')].split(':')[1]
    except ValueError:
        student_telephone = ""
    except Exception as e:
        print "ERROR in student_telephone: ", str(e), "\n\n"
        probs.append(filename+" (student_telephone): "+str(e))
        pass

    try:
        student_email = trimmed_page_content[trimmed_page_content.index('E-Mail ID'):trimmed_page_content.index('Attendance:')].split(':')[1].lower()
    except ValueError:
        student_email = ""
    except Exception as e:
        print "ERROR in student_email: ", str(e), "\n\n"
        probs.append(filename+" (student_email): "+str(e))
        pass

    ## DEBUG: Print stuff. Comment during final run.
    print "Name: ", student_name
    print "Class: ", student_class
    print "DoB: ", student_dob
    print "ID: ", student_id
    print "Address: ", student_address
    print "Mother: ", student_mother
    print "Father: ", student_father
    print "Phone: ", student_telephone
    print "E-mail: ", student_email
    print "\n\n"


    ## XML Stuff begins. Test it out (and format if you want in a text editor) to see the structure.
    student = ET.SubElement(students, "student")
    ET.SubElement(student, "id").text = student_id
    ET.SubElement(student, "name").text = student_name
    ET.SubElement(student, "class").text = student_class
    ET.SubElement(student, "dob").text = student_dob
    ET.SubElement(student, "email").text = student_email
    ET.SubElement(student, "telephone").text = student_telephone
    ET.SubElement(student, "address").text = student_address
    parents = ET.SubElement(student, "parents")
    ET.SubElement(parents, "mother").text = student_mother
    ET.SubElement(parents, "father").text = student_father

    file.close()    # Close the pdf.


## Store probs in file
if probs:
    probsfile = open("probs.txt", "w+")
    for prob in probs:
        probsfile.write(str(prob) + "\n")
    print "Wrote probs.txt\n\n"
    probsfile.close()


## Dump final
print "Final XML to be written to file:\n"
ET.dump(root)                  # DEBUG: Print final xml.
tree = ET.ElementTree(root)
tree.write("out.xml")          # Write xml to file.


### COMMENT: Closing and opening xml object to save memory.


## Sorting XML (by id)
tree = ET.parse("out.xml")
container = tree.find("students")
def getkey(elem):
    return elem.findtext("id")
container[:] = sorted(container, key=getkey)
tree.write("out-sorted.xml")


exit()
