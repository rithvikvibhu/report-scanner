import xml.etree.cElementTree as ET

tree = ET.parse("out.xml")

container = tree.find("students")

def getkey(elem):
    return elem.findtext("id")

# container = tree.find("entries")

container[:] = sorted(container, key=getkey)

tree.write("sorted-out.xml")
# print container