import PyPDF2
import re
import xml.etree.cElementTree as ET
import os

## Init
probs = []
foldername = "debug"
root = ET.Element("data")
students = ET.SubElement(root, "students")

for filename in os.listdir(foldername):
    
    try:
        ## Open PDF
        print foldername, filename
        file = open(foldername+'/'+filename,"rb")
        pdfReader = PyPDF2.PdfFileReader(file)
        
        ## Extract content
        
        pageObj = pdfReader.getPage(0)
        page_content = pageObj.extractText()
        trimmed_page_content = page_content#[289:-56]
        print trimmed_page_content
        
        ## Variables set
        
        student_name = trimmed_page_content[trimmed_page_content.index('Student Profile')+17:trimmed_page_content.index('Section')]
        student_class = trimmed_page_content[trimmed_page_content.index('Section'):trimmed_page_content.index('Date of Birth')]
        student_dob = trimmed_page_content[trimmed_page_content.index('Date of Birth'):trimmed_page_content.index('Admission No')]
        student_id = trimmed_page_content[trimmed_page_content.index('Admission No'):trimmed_page_content.index('Residential Address')]
        student_address = trimmed_page_content[trimmed_page_content.index('Residential Address'):trimmed_page_content.index('Mother')]
        student_mother = trimmed_page_content[trimmed_page_content.index('Mother'):trimmed_page_content.index('Father')-1]
        student_father = trimmed_page_content[trimmed_page_content.index('Father'):trimmed_page_content.index('Telephone Number')]
        student_telephone = trimmed_page_content[trimmed_page_content.index('Telephone Number'):trimmed_page_content.index('E-Mail ID')]
        student_email = trimmed_page_content[trimmed_page_content.index('E-Mail ID'):trimmed_page_content.index('Attendance:')]
        
        # print student_name
        # print student_class
        # print student_dob
        # print student_id
        # print student_address
        # print student_mother
        # print student_father
        # print student_telephone
        # print student_email
        
        
        ## XML Stuff begins
        
        student = ET.SubElement(students, "student")
        ET.SubElement(student, "id").text = student_id[15:]
        ET.SubElement(student, "name").text = student_name[21:].title()
        ET.SubElement(student, "class").text = student_class[9:]
        ET.SubElement(student, "dob").text = student_dob[15:]
        ET.SubElement(student, "email").text = student_email[11:]
        ET.SubElement(student, "telephone").text = student_telephone[18:]
        ET.SubElement(student, "address").text = student_address[21:]
        parents = ET.SubElement(student, "parents")
        ET.SubElement(parents, "mommy").text = student_mother[15:].title()
        ET.SubElement(parents, "daddy").text = student_father[15:].title()
        
    except Exception:
        probs.append(filename+": "+Exception)
        pass


## Store probs

if probs:
    probsfile = open("probs.txt", "w+")
    probsfile.write(probs)
    print "Wrote probs.txt"


## Dump final

ET.dump(root)
tree = ET.ElementTree(root)
tree.write("out.xml")


## Sorting XML

tree = ET.parse("out.xml")
container = tree.find("students")
def getkey(elem):
    return elem.findtext("id")
container[:] = sorted(container, key=getkey)

tree.write("out-sorted.xml")


exit()